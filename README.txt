
==================================================================================
Basic usage
==================================================================================

The FeedAPI Language Filter module relies on the FeedAPI Item Filter, which is
available on Drupal.org: http://drupal.org/project/feedapi_itemfilter

Enable the FeedAPI Item Filter module and the FeedAPI Language Filter module.

You can enable/disable and modify filter settings for each feed. Be sure to
enable the filter that you want to use, as the filters are disabled by default.

You can access the filter settings on each feed page as well as under feed
enabled 'content type' settings. If you modify the filter settings under
'content type' then these settings will become the default settings for the new
items of this content type. So for the keyword filter --  if you use the same
keywords in different feeds then it is a good idea to first fill the keywords
under 'content type' settings and then create the feeds. In this way these
keywords will be already available, when you create a new feed (and obviously
you can modify them individually when you are creating new feeds).

Please note that item filtering will consume time. So it would be a good idea
to keep the filters simple. If you use different filter types on top of each
other, then try to run less time consuming filters first.